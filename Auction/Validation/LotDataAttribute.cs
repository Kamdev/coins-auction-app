﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Auction.Validation
{
    public class LotDataAttribute : ValidationAttribute
    {
        public LotDataAttribute()
        {
            ErrorMessage = "Lot's active time must be in 1 from current moment to 1 year .";
        }

        public override bool IsValid(object value)
        {
            DateTime time = value is DateTime ? (DateTime) value : default;
            
            return time > DateTime.Now && time < DateTime.Now.AddYears(1);
        }
    }
    
}