﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Mvc.Routing;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.AspNetCore.Razor.TagHelpers;

namespace Auction.TagHelpers
{
    [HtmlTargetElement("rating", Attributes = "rating, current-user-name, user-name")]
    public class RatingTagHelper : TagHelper
    {
        public RatingTagHelper(IUrlHelperFactory urlHelperFactory)
        {
            this.urlHelperFactory = urlHelperFactory;
        }
        
        private IUrlHelperFactory urlHelperFactory;
        
        [ViewContext]
        [HtmlAttributeNotBound]
        public ViewContext ViewContext { get; set; }

        [HtmlAttributeName(DictionaryAttributePrefix = "page-url-")]
        public Dictionary<string, object> PageUrlValues { get; set; } = new Dictionary<string, object>();

        public int Rating { get; set; }
        
        public string UserName { get; set; }
        
        public string CurrentUserName { get; set; }
        
        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            IUrlHelper urlHelper = urlHelperFactory.GetUrlHelper(ViewContext);
            output.TagName = "div";
            output.TagMode = TagMode.StartTagAndEndTag;
            
            PageUrlValues["userName"] = UserName;

            byte rating = 1;
            while (rating <= Rating)
            {
                TagBuilder ratingStar = new TagBuilder("span");
                ratingStar.Attributes["class"] = "fa fa-star checked";

                if (UserName == CurrentUserName)
                {
                    ratingStar.Attributes["style"] = "color:orange";
                    output.Content.AppendHtml(ratingStar);
                }
                else
                {
                    TagBuilder reference = new TagBuilder("a");
                    PageUrlValues["value"] = rating;
                    reference.Attributes["href"] = urlHelper.Action("SetAssessment", "Account", PageUrlValues);
                    reference.Attributes["style"] = "color:orange";
                    
                    reference.InnerHtml.AppendHtml(ratingStar);
                    output.Content.AppendHtml(reference);                    
                }

                rating++;
            }

            while (rating <= 5)
            {
                TagBuilder ratingStar = new TagBuilder("span");
                ratingStar.Attributes["class"] = "fa fa-star";

                if (UserName == CurrentUserName)
                {
                    ratingStar.Attributes["style"] = "color:black";
                    output.Content.AppendHtml(ratingStar);
                }
                else
                {
                    TagBuilder reference = new TagBuilder("a");
                    PageUrlValues["value"] = rating;
                    reference.Attributes["href"] = urlHelper.Action("SetAssessment", "Account", PageUrlValues);
                    reference.Attributes["style"] = "color:black";
                
                    reference.InnerHtml.AppendHtml(ratingStar);
                    output.Content.AppendHtml(reference);                    
                }
                
                rating++;
            }
        }
    }
}