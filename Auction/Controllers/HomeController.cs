﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Auction.BLL.Interfaces;
using Auction.BLL.Services;
using Auction.DAL.Entities;
using Microsoft.AspNetCore.Mvc;
using Auction.Models;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace Auction.Controllers
{
    public class HomeController : Controller
    {
        private readonly IWebHostEnvironment appEnvironment;
        private readonly ILotService lotService;
        private readonly IUserService userService;
        private readonly IMapper mapper;
        
        public HomeController(IWebHostEnvironment appEnvironment, IMapper mapper,
            ILotService lotService, IUserService userService)
        {
            this.appEnvironment = appEnvironment;
            this.lotService = lotService;
            this.userService = userService;
            this.mapper = mapper;
        }

        public async Task<IActionResult> Index(string searchingString)
        {
            List<Lot> lots = !string.IsNullOrEmpty(searchingString) ? 
                lotService.GetActiveLots(searchingString) : lotService.GetActiveLots();
            List<LotViewModel> lotViewModels = mapper.Map<List<Lot>, List<LotViewModel>>(lots);

            for (int i = 0; i < lots.Count; i++)
            {
                var user = await userService.GetUserById(lots[i].UserId);
                string directoryName = appEnvironment.WebRootPath + @"\img\lotsimages";
                string pattern = directoryName + @"\" + user.Id + "_" + lots[i].LotId + "_";

                LotImagesLoader loader = new LotImagesLoader();
                var images = loader.FindImages(directoryName, pattern);

                lotViewModels[i].Coin.Images = images.Any() ? images : null;
            }
            
            return View(lotViewModels);
        }

        [HttpGet]
        [Authorize]
        public IActionResult CreateLot()
        {
            ViewBag.Metals = new SelectList(Enum.GetNames(typeof(Metal)));
            return View();
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateLot(CreateLotViewModel model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    ViewBag.Metals = new SelectList(Enum.GetNames(typeof(Metal)));
                    return View();
                }

                if (model is null)
                {
                    throw new ArgumentNullException(nameof(model), "Impossible to create lot, invalid data for lot.");
                }
                
                string userId = userService.GetUserId(this.User);
                Lot lot = mapper.Map<Lot>(model);
                lot.UserId = userId;
                lotService.CreateLot(lot);
                await WriteImages(model.Coin.Images, userId, lot.LotId);
                
                return RedirectToAction("ViewLot", new {lotId = lot.LotId});
            }
            catch (ArgumentNullException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message + " Parameter " + e.ParamName + " is undefined."

                });
            }
        }

        [NonAction]
        private async Task WriteImages(IEnumerable<IFormFile> images, string userId, int lotId)
        {
            if (images is { })
            {
                foreach(var image in images)
                {
                    string path =  appEnvironment.WebRootPath + @"\img\lotsimages" + @"\" + userId + "_" + lotId + "_" + Guid.NewGuid().ToString() + "_" + image.FileName;

                    await using var fileStream = new FileStream(path, FileMode.Create);
                    await image.CopyToAsync(fileStream);
                }
            }
        }

        [HttpGet]
        public async Task<IActionResult> ViewLot(int lotId)
        {
            try
            { 
                Lot lot = lotService.GetLot(lotId);

                var user = await userService.GetUserById(lot.UserId);
                
                string directoryName = appEnvironment.WebRootPath + @"\img\lotsimages";
                string pattern = directoryName + @"\" + user.Id + "_" + lotId + "_";

                LotImagesLoader loader = new LotImagesLoader();
                IEnumerable<string> images = loader.FindImages(directoryName, pattern);
                
                var lotViewModel = mapper.Map<LotViewModel>(lot);
                lotViewModel.UserName = user.UserName;
                lotViewModel.Coin.Images =  images.Any() ? images : null;
                lotViewModel.Phone = user.PhoneNumber;
                ViewBag.OwnerId = user.Id;

                return View(lotViewModel);
            }
            catch (ArgumentNullException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message + " Parameter " + e.ParamName + " is undefined."

                });
            }
            catch (ArgumentException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public IActionResult DeleteLot(int lotId)
        {
            try
            {
                string userId = userService.GetUserId(this.User);
                
                lotService.DeleteLot(lotId, userId);

                string directoryName = appEnvironment.WebRootPath + @"\img\lotsimages";
                string pattern = directoryName + @"\" + userId + "_" + lotId + "_";

                LotImagesRemover remover = new LotImagesRemover();
                remover.FindAndRemoveImages(directoryName, pattern);
                
                return View("LotSuccessfullyDeleted");
            }
            catch (ArgumentNullException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message + " Parameter " + e.ParamName + " is undefined."
                });
            }
            catch (ArgumentException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message
                });
            }
        }

        [Authorize]
        [HttpGet]
        public IActionResult EditLot(int lotId)
        {
            try
            {
                Lot lot = lotService.GetLot(lotId);
                string userId = userService.GetUserId(this.User);

                if (lot.UserId != userId)
                {
                    throw new ArgumentException("Impossible to edit lot. You are not an owner of it.");
                }
                
                ViewBag.Metals = new SelectList(Enum.GetNames(typeof(Metal)));
                ViewBag.lotId = lotId;

                var editLotViewModel = mapper.Map<EditLotViewModel>(lot);
                
                return View(editLotViewModel);
            }
            catch (ArgumentNullException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message + " Parameter " + e.ParamName + " is undefined."
                });
            }
            catch (ArgumentException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message
                });
            }
        }

        [Authorize]
        [HttpPost]
        public async Task<IActionResult> EditLot(EditLotViewModel model, int lotId)
        {
            Lot lot = lotService.GetLot(lotId);
            string userId = userService.GetUserId(this.User);
            
            if (!ModelState.IsValid || model is null || model.Coin is null || lot.UserId != userId)
            {
                return View("Error");
            }
            
            lot.Coin = mapper.Map<Coin>(model.Coin);
            
            lot.Description = model.Description;

            if (model.Coin.Images is { })
            {
                string directoryName = appEnvironment.WebRootPath + @"\img\lotsimages";
                string pattern = directoryName + @"\" + userId + "_" + lotId + "_";
                
                LotImagesRemover remover = new LotImagesRemover();
                remover.FindAndRemoveImages(directoryName, pattern);

                await WriteImages(model.Coin.Images, userId, lotId);
            }
            
            lotService.EditLot(lot);

            return RedirectToAction("ViewLot", new {lotId});
        }
    }
}