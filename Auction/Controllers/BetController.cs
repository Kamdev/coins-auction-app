﻿using System;
using Auction.BLL.Interfaces;
using Auction.DAL.Entities;
using Auction.Infrastructure;
using Auction.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Auction.Controllers
{
    public class BetController : Controller
    {
        private readonly ILotService lotService;
        private readonly IBetService betService;
        private readonly IUserService userService;

        public BetController(ILotService lotService, IBetService betService, IUserService userService)
        {
            this.lotService = lotService;
            this.betService = betService;
            this.userService = userService;
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult CreateBet(int lotId, decimal sum)
        {
            try
            {
                string userId = userService.GetUserId(this.User);
                Lot lot = lotService.GetLot(lotId);

                if (!lotService.IsLotActive(lotId))
                {
                    throw new NonActiveLotException(lot.Name);
                }

                if (userId == lot.UserId)
                {
                    throw new ArgumentException("Owner can't make bets on this lot.");
                }

                betService.CreateBet(userId, lotId, sum);
                lotService.ChangeActualPrice(lotId, sum);

                return RedirectToAction("ViewLot", "Home",new {lotId});
            }
            catch (ArgumentNullException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message + " Parameter " + e.ParamName + " is undefined."
                });
            }
            catch (ArgumentException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message
                });
            }
            catch (NonActiveLotException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.LotName + " isn't active. Please, find other liked lot."
                });
            }
        }
    }
}