﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Auction.BLL.Interfaces;
using Auction.DAL.Entities;
using Auction.Models;
using Auction.BLL.Services;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace Auction.Controllers
{
    public class AccountController : Controller
    {
        private readonly IWebHostEnvironment appEnvironment;
        private readonly SignInManager<User> signInManager;
        private readonly ILotService lotService;
        private readonly IBetService betService;
        private readonly IUserService userService;
        private readonly IMapper mapper;

        public AccountController(IWebHostEnvironment appEnvironment, SignInManager<User> signInManager, IMapper mapper,
            ILotService lotService, IBetService betService, IUserService userService)
        {
            this.signInManager = signInManager;
            this.mapper = mapper;
            this.lotService = lotService;
            this.betService = betService;
            this.userService = userService;
            this.appEnvironment = appEnvironment;
        }

        [HttpGet]
        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel model)
        {
            if (!ModelState.IsValid)
            {
                User user = mapper.Map<User>(model);

                var resultOfCreation = await userService.CreateUser(user, model.Password);
                
                if (resultOfCreation.Succeeded)
                {
                    SendConfirmation(user, model.Email, "Confirm your account", "Confirm sign in: ");
 
                    return View("CheckEmail");
                }
                
                foreach (var error in resultOfCreation.Errors)
                {
                    ModelState.AddModelError(string.Empty, error.Description);
                }
            }

            return View(model);
        }

        [NonAction]
        private async void SendConfirmation(User user, string email, string subject, string message)
        {
            var code = await userService.GenerateEmailConfirmation(user);
            var callbackUrl = Url.Action(
                "ConfirmEmail",
                "Account",
                new { userId = user.Id, code },
                protocol: HttpContext.Request.Scheme);
                    
            EmailService emailService = new EmailService();
            await emailService.SendEmailAsync(email, subject,
                message + $"<a href='{callbackUrl}'>link</a>");
        }
        
        [HttpGet]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            var result = await userService.ConfirmEmail(userId, code);
            
            if (result.Succeeded)
                return RedirectToAction("Index", "Home");
            
            return View("Error", new ErrorViewModel
            {
                ErrorMessage = "Email verification failed. Try again."
            });
        }
        
        [HttpGet]
        public IActionResult Login(string returnUrl = null)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }
 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = userService.GetUserByName(model.UserName);
                if (user is { })
                {
                    if (!await userService.IsUserEmailConfirmed(user))
                    {
                        ModelState.AddModelError(string.Empty, "You didn't confirm your email.");
                        return View(model);
                    }

                    var result =
                        await signInManager.PasswordSignInAsync(model.UserName, model.Password, model.RememberMe, false);
                    if (result.Succeeded)
                    {
                        return !string.IsNullOrEmpty(model.ReturnUrl) && Url.IsLocalUrl(model.ReturnUrl)
                            ? Redirect(model.ReturnUrl) : RedirectToAction("Index", "Home");
                    }
                    
                    ModelState.AddModelError("", "Invalid login and(or) password");
                }
                else
                {
                    ModelState.AddModelError("", "User with such name does not exist.");
                }
            }

            return View(model);
        }
 
        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Logout()
        {
            await signInManager.SignOutAsync();
            HttpContext.Response.Cookies.Delete(".AspNetCore.Cookies");
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult ForgotPassword()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var user = await userService.GetByEmail(model.Email);
            
            if (!await userService.IsUserEmailConfirmed(user))
            {
                return View("ForgotPasswordConfirmation");
            }
 
            SendConfirmation(user, user.Email, "Reset Password", "To reset password follow the link:");
            return View("CheckEmail");
        }
        
        [HttpGet]
        public IActionResult ResetPassword(string code = null)
            => code is null ? View("Error") : View();

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            
            var user = await userService.GetByEmail(model.Email);
            if (user is null)
            {
                return View("ResetPasswordConfirmation");
            }
            
            var result = await userService.ResetPassword(user, model.Code, model.Password);
            if (result.Succeeded)
            {
                return View("ResetPasswordConfirmation");
            }
            
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
            
            return View(model);
        }

        [HttpGet]
        public async Task<IActionResult> ViewProfile(string id)
        {
            var user = userService.GetUserByName(id);

            if (user is null)
            {
                throw new ArgumentException("User with such name doesn't exist.");
            }

            List<Lot> userLots = lotService.GetLots(user.Id);
            List<LotViewModel> lotViewModels = mapper.Map<List<Lot>, List<LotViewModel>>(userLots);
            
            List<Bet> userBets = betService.GetBets(user.Id);
            List<LotViewModel> lotsWithUsersBets = new List<LotViewModel>();
            
            foreach (var bet in userBets)
            {
                Lot lot = lotService.GetLot(bet.LotId);
                string directoryName = appEnvironment.WebRootPath + @"\img\lotsimages";
                string pattern = directoryName + @"\" + lot.UserId + "_" + lot.LotId + "_";

                LotImagesLoader loader = new LotImagesLoader();
                IEnumerable<string> images = loader.FindImages(directoryName, pattern);

                var lotViewModel = mapper.Map<LotViewModel>(lot);
                
                lotViewModel.Coin = new CoinViewModel
                {
                    Images = images.Any() ? images : null
                };

                lotsWithUsersBets.Add(lotViewModel);
            }

            for (int i = 0; i < userLots.Count; i++)
            {
                var owner = await userService.GetUserById(userLots[i].UserId);
                string directoryName = appEnvironment.WebRootPath + @"\img\lotsimages";
                string pattern = directoryName + @"\" + owner.Id + "_" + userLots[i].LotId + "_";

                LotImagesLoader loader = new LotImagesLoader();
                IEnumerable<string> images = loader.FindImages(directoryName, pattern);

                lotViewModels[i].Coin.Images = images.Any() ? images : null;
            }

            ProfileViewModel userModel = mapper.Map<ProfileViewModel>(user);
            userModel.Lots = lotViewModels;
            userModel.UserBets = lotsWithUsersBets;
            ViewBag.UserId = user.Id;
            
            return View(userModel);
        }

        [Authorize]
        [HttpGet]
        public async Task<IActionResult> EditProfile()
        {
            string id = userService.GetUserId(this.User);
            var user = await userService.GetUserById(id);

            return View(mapper.Map<EditProfileViewModel>(user));
        }

        [Authorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditProfile(EditProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            if (model is null)
            {
                throw new ArgumentNullException(nameof(model));
            }
            
            var user = await userService.GetUserById(userService.GetUserId(User));
            
            if (model.AvatarImage is { })
            {
                byte[] imageData;
                    
                using (var binaryReader = new BinaryReader(model.AvatarImage.OpenReadStream()))
                {
                    imageData = binaryReader.ReadBytes((int)model.AvatarImage.Length);
                }
                    
                user.Avatar = imageData;
            }

            user.PhoneNumber = model.PhoneNumber;
            user.UserName = model.UserName;
            user.IsPhoneHidden = model.IsPhoneHidden;

            if (model.Email is { } && model.Email != user.Email)
            {
                SendConfirmation(user, model.Email, "Confirm your account", "Confirm sign in: ");
                    
                await userService.SaveChanges(user);

                return View("CheckEmail");
            }

            await userService.SaveChanges(user);

            return RedirectToAction("ViewProfile", new{id=user.UserName});
        }

        [Authorize]
        public async Task<IActionResult> SetAssessment(string userName, byte value)
        {
            try
            {
                var userTarget = userService.GetUserByName(userName);
                string currentUserId = userService.GetUserId(this.User);

                if (userTarget.Id == currentUserId)
                {
                    throw new ArgumentException("You cannot rate yourself.");
                }

                if (value < 6 && value > 0)
                {
                    await userService.SetAssessment(userTarget.UserName, currentUserId, value);
                }

                return RedirectToAction("ViewProfile", new {id = userTarget.UserName});
            }
            catch (ArgumentException e)
            {
                return View("Error", new ErrorViewModel
                {
                    ErrorMessage = e.Message + " Parameter " + e.ParamName + " is undefined."
                });
            }
        }
    }
}