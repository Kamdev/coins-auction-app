﻿using System;
using Microsoft.AspNetCore.Mvc;

namespace Auction.Controllers
{
    public class ErrorController : Controller
    {
        [Route("Error/{statusCode}")]
        public IActionResult HandleHttpStatusCode(int statusCode)
        {
            switch (statusCode)
            {
                case 404:
                    ViewBag.ErrorMessage = "Resource couldn't be found.";
                    break;
            }
            
            return View("NotFound");
        }
    }
}