﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Auction.Models
{
    public class EditLotViewModel
    {
        [Required]
        [Display(Name = "Lot's name")]
        public string Name { get; set; }

        [Required]
        public CreateCoinViewModel Coin { get; set; }
        
        public string Description { get; set; }
        
        public string ReturnUrl { get; set; }
    }
}