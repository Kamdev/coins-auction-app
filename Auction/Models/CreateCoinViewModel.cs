﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Auction.Models
{
    public class CreateCoinViewModel
    {
        [Required]
        [Display(Name = "Coin name")]
        public string Name { get; set; }
        
        [Range(0,2021,ErrorMessage = "Year of minting must be in range from 0 to 2021")]
        public int? Year { get; set; }
        
        [RegularExpression(@"\D+", ErrorMessage = "Country name couldn't include numeric symbol")]
        public string Country { get; set; }
        
        [Required]
        public string Metal { get; set; }
        
        [Range(0.1, 10_000, ErrorMessage = "Invalid weight of coin.")]
        public double? Weight { get; set; }

        public List<IFormFile> Images { get; set; }
    }
}