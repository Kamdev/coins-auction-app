﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auction.DAL.Entities;

namespace Auction.Models
{
    public class LotViewModel
    {
        public int LotId { get; set; }
        
        public string Name { get; set; }
        
        public CoinViewModel Coin { get; set; }
        
        public decimal PrimeBetPrice { get; set; }

        public List<BetViewModel> Bets { get; set; }
        
        public decimal ActualBetPrice { get; set; }
        
        public string UserName { get; set; }
        
        public DateTime FinishingDate { get; set; }
        
        public string Description { get; set; }
        
        public bool IsActive { get; set; }
        
        public string Phone { get; set; }
    }
}