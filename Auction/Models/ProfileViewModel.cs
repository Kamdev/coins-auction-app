﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Auction.DAL.Entities;
using Microsoft.AspNetCore.Http;
using Org.BouncyCastle.Asn1.X509;

namespace Auction.Models
{
    public class ProfileViewModel
    {
        public string Email { get; set; }
        
        [Display(Name = "Phone")]
        public string PhoneNumber { get; set; }
        
        
        [Display(Name = "Phone hidden")]
        private bool IsPhoneHidden { get; set; }
        
        [Display(Name = "User name")]
        public string UserName { get; set; }
        
        public List<LotViewModel> Lots { get; set; }
        
        [Display(Name = "Active bets")]
        public List<LotViewModel> UserBets { get; set; }

        public int Rating { get; set; }

        public byte[] Avatar { get; set; }
    }
}