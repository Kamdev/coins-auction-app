﻿namespace Auction.Models
{
    public class BetViewModel
    {
        public string UserId { get; set; }
        public decimal Sum { get; set; }
    }
}