﻿using System.ComponentModel.DataAnnotations;

namespace Auction.Models
{
    public class ForgotPasswordModel
    {
        [Required]
        public string Email { get; set; }
    }
}