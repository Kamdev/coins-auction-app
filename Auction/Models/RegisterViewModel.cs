﻿using System.ComponentModel.DataAnnotations;

namespace Auction.Models
{
    public class RegisterViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
        
        [Required]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
 
        [Required]
        [Compare("Password", ErrorMessage = "Invalid password")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        public string PasswordConfirm { get; set; }
        
        [StringLength(13, ErrorMessage = "Incorrect number for this region")]
        [RegularExpression(@"^\+375\d{9}", ErrorMessage = "Incorrect number for this region")]
        [Display(Name = "Phone")]
        public string PhoneNumber { get; set; }
    }
}