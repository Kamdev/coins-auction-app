﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Auction.DAL.Entities;
using Auction.Validation;

namespace Auction.Models
{
    public class CreateLotViewModel
    {
        [Required]
        [Display(Name = "Lot's name")]
        public string Name { get; set; }
        
        [Display(Name = "Prime bet's sum")]
        [Required]
        [Range(0, 10_000, ErrorMessage = "Prime bet must be in range from 0 to 10 000")]
        public decimal PrimeBetPrice { get; set; }
        
        [Display(Name = "Finishing date")]        
        [LotData]
        [Required(ErrorMessage = "Select valid date.")]
        public DateTime FinishingDate { get; set; }
        
        [Required]
        public CreateCoinViewModel Coin { get; set; }
        
        public string Description { get; set; }
    }
}