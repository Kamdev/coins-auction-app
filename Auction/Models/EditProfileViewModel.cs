﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace Auction.Models
{
    public class EditProfileViewModel
    {
        public string Email { get; set; }

        [Display(Name = "User name")]
        public string UserName { get; set; }
        
        [StringLength(13, ErrorMessage = "Incorrect number for this region")]
        [RegularExpression(@"^\+375\d{9}", ErrorMessage = "Incorrect number for this region")]
        [Display(Name = "Phone")]
        public string PhoneNumber { get; set; }
        
        [Display(Name = "Hide for other users")]
        public bool IsPhoneHidden { get; set; }

        public IFormFile AvatarImage { get; set; }
    }
}