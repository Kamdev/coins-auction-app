﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Auction.Models
{
    public class ResetPasswordViewModel
    {
        [Required]
        public string Email { get; set; }
 
        [Required]
        [DataType(DataType.Password)]
        [Display(Name="New password")]
        public string Password { get; set; }
 
        [DataType(DataType.Password)]
        [Display(Name = "Confirm new password")]
        public string ConfirmPassword { get; set; }
 
        public string Code { get; set; }
    }
}