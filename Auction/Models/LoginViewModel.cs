﻿using System.ComponentModel.DataAnnotations;

namespace Auction.Models
{
    public class LoginViewModel
    {
        [Display(Name = "User name")]
        [Required]
        public string UserName { get; set; }
         
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }
         
        [Display(Name = "Save password")]
        public bool RememberMe { get; set; }
         
        public string ReturnUrl { get; set; }
    }
}