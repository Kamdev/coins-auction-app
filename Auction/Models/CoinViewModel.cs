﻿using System.Collections.Generic;

namespace Auction.Models
{
    public class CoinViewModel
    {
        public string Name { get; set; }
        
        public string Year { get; set; }
        
        public string Country { get; set; }
        
        public string Metal { get; set; }
        
        public string Weight { get; set; }
        
        public IEnumerable<string> Images { get; set; }
    }
}