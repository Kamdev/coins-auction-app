﻿using Auction.DAL.Entities;
using Auction.Models;
using AutoMapper;

namespace Auction.Infrastructure
{
    public class AutoMapping : Profile
    {
        public AutoMapping()
        {
            CreateMap<CreateCoinViewModel, Coin>().ReverseMap();
            CreateMap<CreateLotViewModel, Lot>();
            CreateMap<Coin, CoinViewModel>()
                .ForMember(dest=>dest.Year,
                opt=>opt.MapFrom(
                    src=> src.Year == null ? "-" : src.Year.ToString()))
                .ForMember(dest=>dest.Country,
                    opt=>opt.MapFrom(
                        src=>src.Country ?? "-"))
                .ForMember(dest=>dest.Weight,
                    opt=>opt.MapFrom(
                        src=> src.Weight == null ? "-" : src.Weight.ToString()))
                .ForMember(dest=>dest.Metal,
                    opt=>opt.MapFrom(
                        src=>src.Metal ?? "-"));
            CreateMap<Lot, LotViewModel>();
            CreateMap<EditLotViewModel, Lot>().ReverseMap();
            CreateMap<User, ProfileViewModel>();
            CreateMap<User, EditProfileViewModel>().ReverseMap();
            CreateMap<RegisterViewModel, User>();
            CreateMap<Bet, BetViewModel>();
        }
    }
}