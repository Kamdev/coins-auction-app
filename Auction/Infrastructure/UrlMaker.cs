﻿using Microsoft.AspNetCore.Http;

namespace Auction.Infrastructure
{
    public static class UrlMaker
    {
        public static string GetPathAndQuery(this HttpRequest request)
        {
            return request.QueryString.HasValue ? $"{request.Path}{request.QueryString}" : request.Path.ToString();
        }
    }
}