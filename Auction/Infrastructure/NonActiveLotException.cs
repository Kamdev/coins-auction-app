﻿using System;

namespace Auction.Infrastructure
{
    public class NonActiveLotException : Exception
    {
        public string LotName { get; }

        public NonActiveLotException(string lotName) : base()
        {
            LotName = lotName;
        }
    }
}