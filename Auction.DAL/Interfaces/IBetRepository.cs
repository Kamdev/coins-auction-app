﻿using System.Linq;
using Auction.DAL.Entities;

namespace Auction.DAL.Interfaces
{
    public interface IBetRepository
    {
        public IQueryable<Bet> Bets { get; }

        void CreateBet(Bet bet);

        void DeleteBet(Bet bet);
    }
}