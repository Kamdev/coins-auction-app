﻿using System.Linq;
using Auction.DAL.Entities;

namespace Auction.DAL.Interfaces
{
    public interface ICoinRepository
    {
        public IQueryable<Coin> Coins { get; }

        void EditCoin(Coin coin);

        void CreateCoin(Coin coin);

        void DeleteCoin(Coin coin);
    }
}