﻿using System.Linq;
using Auction.DAL.Entities;

namespace Auction.DAL.Interfaces
{
    public interface ILotRepository
    {
        public IQueryable<Lot> Lots { get; }

        void AddLot(Lot lot);

        void DeleteLot(Lot lot);

        void EditLot(Lot lot);

        bool SetBet(Bet bet);
    }
}