﻿namespace Auction.DAL.Entities
{
    public enum Metal
    {
        Other,
        Silver,
        Gold,
        Platinum,
        Bronze,
        Copper,
        Aluminum,
        Zinc,
        Lead,
        Iron
    }
}