﻿using System;
using System.Collections.Generic;
using Auction.DAL.Interfaces;

namespace Auction.DAL.Entities
{
    public class Lot
    {
        public int LotId { get; set; }
        
        public string Name { get; set; }
        
        public Coin Coin { get; set; }
        
        public decimal PrimeBetPrice { get; set; }

        public List<Bet> Bets { get; set; } = new List<Bet>();
        
        public decimal ActualBetPrice { get; set; }
        
        public string UserId { get; set; }
        
        public User User { get; set; }
        
        public DateTime FinishingDate { get; set; }
        
        public string Description { get; set; }
        
        public bool IsActive { get; set; }
    }
}