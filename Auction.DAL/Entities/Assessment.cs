﻿namespace Auction.DAL.Entities
{
    public class Assessment
    {
        public int AssessmentId { get; set; }
        
        public byte Value { get; set; }
        
        public string VotingUserId { get; set; }
        
        public string TargetUserId { get; set; }
        
        public User TargetUser { get; set; }
    }
}