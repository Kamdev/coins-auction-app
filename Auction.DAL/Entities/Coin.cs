﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Auction.DAL.Entities
{
    public class Coin
    {
        public int CoinId { get; set; }
        
        public int LotId { get; set; }
        
        public Lot Lot { get; set; }
        
        public string Name { get; set; }
        
        public int? Year { get; set; }
        
        public string Country { get; set; }
        
        public string Metal { get; set; }
        
        public double? Weight { get; set; }
    }
}