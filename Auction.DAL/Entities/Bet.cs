﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Auction.DAL.Entities
{
    public class Bet
    {
        public int BetId { get; set; }

        [ForeignKey("LotId")]
        public int LotId { get; set; }
        
        public Lot Lot { get; set; }
        
        [ForeignKey("ParticipantId")]
        public string UserId { get; set; }
        
        public User User { get; set; }
        
        public decimal Sum { get; set; }
    }
}