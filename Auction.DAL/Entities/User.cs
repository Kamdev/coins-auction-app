﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Identity;

namespace Auction.DAL.Entities
{
    public class User : IdentityUser
    {
        public List<Lot> Purchases { get; set; }

        public List<Lot> Lots { get; set; }

        public List<Bet> ActiveBets { get; set; }
        
        public bool IsPhoneHidden { get; set; }

        public byte[] Avatar { get; set; }
        
        public List<Assessment> Assessments { get; set; }

        public byte Rating
        {
            get
            {
                double sum = 0;
                byte average = 0;
                if (Assessments is { } && Assessments.Any())
                {
                    foreach (var assessment in Assessments)
                    {
                        sum +=assessment.Value;
                    }

                    average = (byte)Math.Round(sum / Assessments.Count, 0, MidpointRounding.AwayFromZero);
                }

                return average;
            }
        }
    }
}