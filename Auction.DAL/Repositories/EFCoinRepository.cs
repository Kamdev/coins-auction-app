﻿using System.Linq;
using Auction.DAL.EF;
using Auction.DAL.Entities;
using Auction.DAL.Interfaces;

namespace Auction.DAL.Repositories
{
    public class EFCoinRepository : ICoinRepository
    {
        private ApplicationContext _context;
        
        public EFCoinRepository(ApplicationContext context)
        {
            _context = context;
        }
        
        public IQueryable<Coin> Coins => _context.Coins;

        public void EditCoin(Coin coin)
        {
            _context.SaveChanges();
        }

        public void CreateCoin(Coin coin)
        {
            if (coin is {})
            {
                _context.Coins.Add(coin);
                _context.SaveChanges();
            }
        }
        
        public void DeleteCoin(Coin coin)
        {
            if (coin is {})
            {
                _context.Coins.Remove(coin);
                _context.SaveChanges(); 
            }
        }
    }
}