﻿using System.Linq;
using Auction.DAL.EF;
using Auction.DAL.Entities;
using Auction.DAL.Interfaces;

namespace Auction.DAL.Repositories
{
    public class EFLotRepository : ILotRepository
    {
        private ApplicationContext _context;

        public EFLotRepository(ApplicationContext context)
        {
            _context = context;
        }

        public IQueryable<Lot> Lots => _context.Lots;
        public void AddLot(Lot lot)
        {
            _context.Lots.Add(lot);
            _context.SaveChanges();
        }

        public void DeleteLot(Lot lot)
        {
            if (lot is { })
            {
                _context.Lots.Remove(lot);
            }

            _context.SaveChanges();
        }

        public void EditLot(Lot lot)
        {
            _context.SaveChanges();
        }

        public bool SetBet(Bet bet)
        {
            Lot lot = _context.Lots.First(l => l.LotId == bet.LotId);

            bool isSetted = false;

            if (lot is { } && bet is { })
            {
                lot.ActualBetPrice = bet.Sum;

                lot.Bets.Add(bet);

                isSetted = true;
            }
            
            _context.SaveChanges();

            return isSetted;
        }
    }
}