﻿using System.Linq;
using Auction.DAL.EF;
using Auction.DAL.Entities;
using Auction.DAL.Interfaces;

namespace Auction.DAL.Repositories
{
    public class EFBetRepository : IBetRepository
    {
        private ApplicationContext _context;
        
        public EFBetRepository(ApplicationContext context)
        {
            _context = context;
        }


        public IQueryable<Bet> Bets => _context.Bets;
        public void CreateBet(Bet bet)
        {
            if (bet is {})
            {
                _context.Bets.Add(bet);
                _context.SaveChanges();
            }
        }

        public void DeleteBet(Bet bet)
        {
            if (bet is {})
            {
                _context.Bets.Remove(bet);
                _context.SaveChanges(); 
            }
        }
    }
}