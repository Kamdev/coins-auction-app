﻿using Auction.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace Auction.DAL.EF
{
    public class ApplicationContext : IdentityDbContext<User>
    {
        public DbSet<Lot> Lots { get; set; }
        
        public DbSet<Bet> Bets { get; set; }
        
        public DbSet<Coin> Coins { get; set; }
        
        public DbSet<Assessment> Assessments { get; set; }
        
        public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {        
            builder.Entity<Lot>()
                .HasOne(l => l.User)
                .WithMany(u => u.Lots).HasForeignKey(l => l.UserId);
            builder.Entity<Assessment>().HasOne(a => a.TargetUser)
                .WithMany(u => u.Assessments).HasForeignKey(a => a.TargetUserId);
            base.OnModelCreating(builder);
        }
    }
}