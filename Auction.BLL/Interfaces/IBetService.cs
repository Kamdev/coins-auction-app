﻿using System.Collections.Generic;
using Auction.DAL.Entities;

namespace Auction.BLL.Interfaces
{
    public interface IBetService
    {
        public void CreateBet(string userId, int lotId, decimal sum);

        public List<Bet> GetBets(int lotId);

        public List<Bet> GetBets(string userId);
    }
}