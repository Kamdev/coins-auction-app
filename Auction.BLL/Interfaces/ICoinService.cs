﻿using System;
using Auction.DAL.Entities;

namespace Auction.BLL.Interfaces
{
    public interface ICoinService
    {
        public Coin GetCoin(int lotId);
        
        public void CreateCoin(Coin coin);

        public void EditCoin(Coin coin);
    }
}