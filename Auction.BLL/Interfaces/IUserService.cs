﻿using System.Security.Claims;
using System.Threading.Tasks;
using Auction.DAL.Entities;
using Microsoft.AspNetCore.Identity;

namespace Auction.BLL.Interfaces
{
    public interface IUserService
    {
        public Task<IdentityResult> CreateUser(User user, string password);
        
        public Task<IdentityResult> ConfirmEmail(string userId, string code);
        
        public Task<IdentityResult> ResetPassword(User user, string code, string password);

        public Task<IdentityResult> SaveChanges(User user);

        public string GetUserId(ClaimsPrincipal user);

        public Task<User> GetUserById(string id);

        public Task<User> GetByEmail(string email);

        public /*Task<User>*/User GetUserByName(string name);

        public Task<bool> IsUserEmailConfirmed(User user);

        public Task<string> GeneratePasswordReset(User user);
        
        public Task<string> GenerateEmailConfirmation(User user);

        public Task SetAssessment(string targetId, string voterId, byte value);
    }
}