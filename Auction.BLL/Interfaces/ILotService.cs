﻿using System.Collections.Generic;
using Auction.DAL.Entities;

namespace Auction.BLL.Interfaces
{
    public interface ILotService
    {
        public List<Lot> GetLots();
        
        public List<Lot> GetLots(string userId);

        public Lot GetLot(int lotId);

        public void ChangeActualPrice(int lotId, decimal sum);

        public void DeleteLot(int lotId, string userId);

        public bool IsLotActive(int lotId);

        public void CreateLot(Lot lot);

        public void EditLot(Lot lot);

        public List<Lot> GetActiveLots();
        
        public List<Lot> GetActiveLots(string name);
    }
}