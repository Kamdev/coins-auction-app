﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auction.BLL.Interfaces;
using Auction.DAL.Interfaces;
using Auction.DAL.Entities;

namespace Auction.BLL.Services
{
    public class BetService : IBetService
    {
        private IBetRepository betRepository;

        public BetService(IBetRepository betRepository)
        {
            this.betRepository = betRepository ?? throw new ArgumentNullException(nameof(betRepository));
        }

        public List<Bet> GetBets(int lotId) => betRepository.Bets.Where(b => b.LotId == lotId).ToList();

        public List<Bet> GetBets(string userId) 
            => userId is {} ? betRepository.Bets.Where(b => b.UserId == userId).ToList()
                : throw new ArgumentNullException(userId);

        public void CreateBet(string userId, int lotId, decimal sum)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId), "Invalid user data.");
            }

            if (lotId < 1)
            {
                throw new ArgumentException("Lot id must be more 0.");
            }

            if (sum <= 0)
            {
                throw new ArgumentException("Bet must be more zero.");
            }

            Bet bet = new Bet
            {
                LotId = lotId,
                UserId = userId,
                Sum = sum,
            };
            
            betRepository.CreateBet(bet);
        }
    }
}