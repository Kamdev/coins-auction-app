﻿using System;
using System.Collections.Generic;
using System.IO;

namespace Auction.BLL.Services
{
    public class LotImagesLoader
    {
        public IEnumerable<string> FindImages(string directoryName, string pattern)
        {
            if (string.IsNullOrEmpty(directoryName))
            {
                throw new ArgumentNullException(nameof(directoryName));
            }
            
            List<string> lotImages = new List<string>();

            if (Directory.Exists(directoryName))
            {
                IEnumerable<string> files = Directory.GetFiles(directoryName);
                
                foreach (string path in files)
                {
                    if (path.StartsWith(pattern))
                    {
                        string image = path;
                        string imageWithReplaceSymbols = image.Replace('\\','/');
                        lotImages.Add(imageWithReplaceSymbols.Substring(42));
                    }
                }
            }
            return lotImages;
        }
    }
}