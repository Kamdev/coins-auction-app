﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Auction.BLL.Interfaces;
using Auction.DAL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace Auction.BLL.Services
{
    public class UserService : IUserService
    {
        private UserManager<User> userManager;

        public UserService(UserManager<User> userManager)
        {
            this.userManager = userManager;
        }
        
        public async Task<IdentityResult> CreateUser(User user, string password)
        {
            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (password is null)
            {
                throw new ArgumentNullException(nameof(password));
            }
            
            return await userManager.CreateAsync(user, password);
        }

        public async Task<IdentityResult> ConfirmEmail(string userId, string code)
        {
            if (string.IsNullOrEmpty(userId))
            {
                throw new ArgumentNullException(nameof(userId));
            }

            if (code is null)
            {
                throw new ArgumentNullException(nameof(code));
            }
            
            var user = await userManager.FindByIdAsync(userId);
            if (user is null)
            {
                throw new ArgumentNullException(nameof(user));
            }
            
            return await userManager.ConfirmEmailAsync(user, code);
        }

        public async Task<IdentityResult> ResetPassword(User user, string code, string password)
        {
            return await userManager.ResetPasswordAsync(user, code, password);
        }

        public async Task<IdentityResult> SaveChanges(User user) => user is {} ? await userManager.UpdateAsync(user)
        : throw new ArgumentNullException(nameof(user));

        public string GetUserId(ClaimsPrincipal user) => user is {} ?userManager.GetUserId(user) 
            : throw new ArgumentNullException(nameof(user));

        public async Task<User> GetUserById(string id) => !string.IsNullOrEmpty(id.Trim()) ?
            await userManager.FindByIdAsync(id) : throw new ArgumentNullException(nameof(id));

        public Task<User> GetByEmail(string email) => !string.IsNullOrEmpty(email) ? 
            userManager.FindByEmailAsync(email) : throw new ArgumentNullException(nameof(email));

        public User GetUserByName(string name) => !string.IsNullOrEmpty(name) ?
            userManager.Users.Include(u=>u.Assessments).First(u=>u.UserName == name) : throw new ArgumentNullException(nameof(name));

        public async Task<bool> IsUserEmailConfirmed(User user) => user is { }
            ? await userManager.IsEmailConfirmedAsync(user)
            : throw new ArgumentNullException(nameof(user));
        
        public async Task<string> GeneratePasswordReset(User user)=> user is { }
            ? await userManager.GeneratePasswordResetTokenAsync(user)
            : throw new ArgumentNullException(nameof(user));

        public async Task<string> GenerateEmailConfirmation(User user) => user is { }
            ? await userManager.GenerateEmailConfirmationTokenAsync(user)
            : throw new ArgumentNullException(nameof(user));

        public async Task SetAssessment(string userName, string voterId, byte value)
        {
            var targetUser = GetUserByName(userName);
            
            targetUser.Assessments ??= new List<Assessment>();

            var assessment = targetUser.Assessments.FirstOrDefault(a=>a.VotingUserId == voterId);

            if (assessment is null)
            {
                targetUser.Assessments.Add(new Assessment
                {
                    Value = value,
                    VotingUserId = voterId,
                    TargetUserId = targetUser.Id
                });                
            }
            else
            {
                assessment.Value = value;
            }
            
            await SaveChanges(targetUser);
        }
    }
}