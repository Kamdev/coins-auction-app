﻿using System;
using System.Linq;
using Auction.BLL.Interfaces;
using Auction.DAL.Entities;
using Auction.DAL.Interfaces;

namespace Auction.BLL.Services
{
    public class CoinService : ICoinService
    {
        private ICoinRepository coinRepository;

        public CoinService(ICoinRepository coinRepository)
        {
            this.coinRepository = coinRepository;
        }

        public Coin GetCoin(int lotId) => coinRepository.Coins.First(c => c.LotId == lotId);

        public void CreateCoin(Coin coin)
        {
            if (coin is null)
            {
                throw new ArgumentNullException(nameof(coin), "Coin is null.");
            }
            
            coinRepository.CreateCoin(coin);
        }

        public void EditCoin(Coin coin)
        {
            coinRepository.EditCoin(coin);
        }
    }
}