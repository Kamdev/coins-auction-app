﻿using System;
using System.Collections.Generic;
using System.Linq;
using Auction.BLL.Interfaces;
using Auction.DAL.Interfaces;
using Auction.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Auction.BLL.Services
{
    public class LotService : ILotService
    {
        private ILotRepository lotRepository;
        
        public LotService(ILotRepository lotRepository)
        {
            this.lotRepository = lotRepository;
        }

        public List<Lot> GetLots() => lotRepository.Lots.Include(l=>l.Bets).ToList();

        public List<Lot> GetLots(string userId)
        {
            if (userId is null)
            {
                throw new ArgumentNullException(nameof(userId));
            }
            
            ResetActivities();
            
            var userLots = lotRepository.Lots.Where(l => l.UserId == userId).Include(l=>l.Coin).ToList();
            List<Lot> sortedLots =  userLots.OrderBy(l=>l.IsActive).ThenBy(x=> x.FinishingDate).ToList();
            sortedLots.Reverse();
            return sortedLots;
        }

        private void ResetActivities()
        {            
            if (lotRepository.Lots is { } && lotRepository.Lots.Any())
            {
                List<Lot> lots = lotRepository.Lots.ToList();
                int count = lots.Count;
                for (int i = 0; i < count; i++)
                {
                    if (DateTime.Now >= lots[i].FinishingDate)
                    {
                        lots[i].IsActive = false;
                        lotRepository.EditLot(lots[i]);
                    }
                }
            }
        }

        public List<Lot> GetActiveLots()
        {
            List<Lot> activeLots = new List<Lot>();
            
            ResetActivities();
            activeLots = lotRepository.Lots.Where(l => l.IsActive == true).Include(l=>l.Coin).ToList();
            
            activeLots.Sort((x, y) => x.FinishingDate.CompareTo(y.FinishingDate));

            return activeLots;
        }

        public List<Lot> GetActiveLots(string name)
        {
            List<Lot> activeLots = GetActiveLots();

            if (name is null)
            {
                throw new ArgumentNullException(nameof(name));
            }

            return activeLots.Where(l => l.Name.Contains(name, StringComparison.OrdinalIgnoreCase)).ToList();
        }
        
        public Lot GetLot(int lotId)
        {
            if (lotId < 1)
            {
                throw new ArgumentException("Such lot doesn't exist.");
            }
            
            Lot lot = lotRepository.Lots.Include(l=>l.Bets).Include(l=>l.Coin).FirstOrDefault(l => l.LotId == lotId);

            if (lot is null)
            {
                throw new ArgumentNullException(nameof(lot),"Sorry, this lot doesn't exist.");
            }
            
            if (lot.FinishingDate <= DateTime.Now)
            {
                lot.IsActive = false;
            }
            
            return lot;
        }

        public void ChangeActualPrice(int lotId, decimal sum)
        {
            Lot lot = GetLot(lotId);
            
            if (sum > lot.ActualBetPrice)
            {
                lot.ActualBetPrice = sum;
                lotRepository.EditLot(lot);
            }
        }

        public void DeleteLot(int lotId, string userId)
        {
            Lot lot = GetLot(lotId);

            if (userId is null)
            {
                throw new ArgumentNullException(nameof(userId));
            }

            if (userId != lot.UserId)
            {
                throw new ArgumentException("Impossible to delete lot. You are not owner of the it.");
            }

            lotRepository.DeleteLot(lot);
        }

        public bool IsLotActive(int lotId) => GetLot(lotId).IsActive;

        public void EditLot(Lot lot)
        {
            if (lot is null)
            {
                throw new ArgumentNullException(nameof(lot));
            }
            
            lotRepository.EditLot(lot);
        }

        public void CreateLot(Lot lot)
        {
            if (lot is null)
            {
                throw new ArgumentNullException(nameof(lot), "Impossible create undefined lot.");
            }
            
            lot.ActualBetPrice = 0;
            lot.IsActive = true;
            
            lotRepository.AddLot(lot);
        }
    }
}