﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Auction.BLL.Services
{
    public class LotImagesRemover
    {
        public void FindAndRemoveImages(string directoryName, string pattern)
        {
            if (string.IsNullOrEmpty(directoryName))
            {
                throw new ArgumentNullException(nameof(directoryName));
            }

            if (Directory.Exists(directoryName))
            {
                IEnumerable<string> removingFiles =
                    ReceiveRemovingFileNames(Directory.GetFiles(directoryName), pattern);
                RemoveImages(removingFiles);
            }
        }

        private IEnumerable<string> ReceiveRemovingFileNames(IEnumerable<string> files, string pattern)
        {
            List<string> removingFiles = new List<string>();

            if (files is null)
            {
                throw new ArgumentNullException(nameof(files));
            }
            
            foreach (string path in files)
            {
                if (path.StartsWith(pattern))
                {
                    removingFiles.Add(path);
                }
            }

            return removingFiles;
        }

        private void RemoveImages(IEnumerable<string> paths)
        {
            if (paths is null)
            {
                throw new ArgumentNullException(nameof(paths));
            }
            
            foreach (var path in paths)
            {
                FileInfo file = new FileInfo(path);
                        
                if (file.Exists)
                {
                    file.Delete();
                }
            }
        }
    }
}